from time import sleep
from selenium.common.exceptions import NoSuchElementException


class Hotel(object):
    def __init__(self, name, price):
        self.name = name
        self.price = price


class SearchResultsPage(object):

    def __init__(self, driver):
        self.driver = driver

    def click_on_price_range(self, number):
        self.driver.find_element_by_xpath('//a[@data-id="pri-%d"]' % number).click()

    def get_upper_range_value(self, number):
        return int(self.driver.find_element_by_xpath('//a[@data-id="pri-%d"]' % number).get_attribute('data-value'))

    # дети не проверяются
    def is_hotel_price_within_price_range(self, price, max_price, count_person, days, count_rooms):
        return price < max_price * count_person * days * count_rooms

    def get_hotel_with_max_price(self, hotels):
        max_hotel = Hotel('test', 0)
        for hotel in hotels:
            if max_hotel.price < hotel.price:
                max_hotel = hotel
        return max_hotel

    def open_hotel_page(self, hotel):
        while self.driver.find_elements_by_xpath('//span[contains(., "%s")]' % hotel.name).__len__() == 0:
            self.driver.find_element_by_xpath('//a[@title="Предыдущая страница"]').click()
            sleep(2)  # TODO wait progressbar
        self.driver.find_element_by_xpath('//span[contains(., "%s")]' % hotel.name).click()

    def get_list_hotels(self):
        hotels = []
        flag = True
        while flag:
            sleep(2)  # TODO wait progressbar
            for i in range(1, self.driver.find_elements_by_xpath(
                    '//div[@class="sr_item_content sr_item_content_slider_wrapper "]').__len__()):
                name = self.driver.find_element_by_xpath(
                    f'(//div[@class="sr_item_content sr_item_content_slider_wrapper "])[{i:d}]//a[@class="hotel_name_link url"]').text
                name = name[:name.find('\n')]

                try:
                    price = int(self.driver.find_element_by_xpath(
                        f'(//div[@class="sr_item_content sr_item_content_slider_wrapper "])[{i:d}]//div[@class="bui-price-display__value prco-inline-block-maker-helper"]').text.replace(
                        ' ', '').replace('руб.', ''))
                except NoSuchElementException:
                    price = 0

                hotels.append(Hotel(name, price))

            try:
                self.driver.find_element_by_xpath('//a[@title="Следующая страница"]').click()
            except NoSuchElementException:
                flag = False

        return hotels


class HomePage(object):

    def __init__(self, driver):
        self.driver = driver

    def input_direction(self, direction):
        self.driver.find_element_by_xpath('//input[@id="ss"]').send_keys(direction)

    def input_travel_date(self, date_of_arrival, departure_date):
        self.driver.find_element_by_xpath('//div[@class="xp__dates-inner"]').click()
        self.driver.find_element_by_xpath('//td[@data-date="%s"]' % date_of_arrival).click()
        self.driver.find_element_by_xpath('//td[@data-date="%s"]' % departure_date).click()

    def input_number_of_rooms_and_guests(self, number_of_adults, number_of_children, number_of_rooms):
        self.driver.find_element_by_xpath('//div[@class="xp__input-group xp__guests"]').click()

        self.__input_guests(number_of_adults, "sb-group__field-adults")
        self.__input_guests(number_of_children, "sb-group-children ")
        self.__input_guests(number_of_rooms, "sb-group__field-rooms")

    def click_button_check_prices(self):
        self.driver.find_element_by_xpath('//*[text()="Проверить цены"]').click()

    def __input_guests(self, required_amount, guests):
        current_amount = int(self.driver.find_element_by_xpath(
            '//div[@class="sb-group__field %s"]//input' % guests).get_attribute("value"))
        while required_amount != current_amount:
            if required_amount < current_amount:
                self.driver.find_element_by_xpath(
                    "//div[@class='sb-group__field %s']//span[text()='-']" % guests).click()
            else:
                self.driver.find_element_by_xpath(
                    "//div[@class='sb-group__field %s']//span[text()='+']" % guests).click()
            current_amount = int(self.driver.find_element_by_xpath(
                '//div[@class="sb-group__field %s"]//input' % guests).get_attribute("value"))
