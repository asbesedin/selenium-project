import unittest
from datetime import datetime
from datetime import timedelta
from selenium.webdriver.chrome.webdriver import WebDriver
from page import HomePage, SearchResultsPage


class BookingTesting(unittest.TestCase):
    direction = 'Самара, Самарская область, Россия'
    days = 4
    adults = 2
    children = 0
    rooms = 1
    number_price_range = 2
    date_of_arrival = datetime.now().strftime("%Y-%m-%d")
    departure_date = (datetime.now() + timedelta(days=days)).strftime('%Y-%m-%d')

    def setUp(self):
        self.driver = WebDriver(executable_path='your_path_to_driver//chromedriver.exe')
        self.driver.maximize_window()
        self.driver.get('https://www.booking.com/index.ru.html')
        self.driver.implicitly_wait(10)

    def tearDown(self):
        self.driver.close()

    def testSearch(self):
        home = HomePage(self.driver)
        home.input_direction(self.direction)
        home.input_travel_date(self.date_of_arrival, self.departure_date)
        home.input_number_of_rooms_and_guests(self.adults, self.children, self.rooms)
        home.click_button_check_prices()

        search_result = SearchResultsPage(self.driver)
        upper_range_value = search_result.get_upper_range_value(self.number_price_range)
        search_result.click_on_price_range(self.number_price_range)
        hotels = search_result.get_list_hotels()

        for hotel in hotels:
            assert search_result.is_hotel_price_within_price_range(hotel.price, upper_range_value, self.adults, self.days, self.rooms)

        hotel_with_max_price = search_result.get_hotel_with_max_price(hotels)
        search_result.open_hotel_page(hotel_with_max_price)

        self.driver.switch_to.window(self.driver.window_handles[1])
        self.driver.get_screenshot_as_file('test.png')
